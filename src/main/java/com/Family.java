
package com;

import java.util.ArrayList;
import java.util.List;

public class Family {
    private Human mother;
    private Human father;
    static List<Human> children = new ArrayList<>();

    private List<Human> children1 = new ArrayList<>();


    Pet pet;

    public static void setChildren(List<Human> children) {
        Family.children = children;
    }

    public List<Human> getChildren1() {
        return children1;
    }

    public void setChildren1(List<Human> children1) {
        this.children1 = children1;
    }

    public Family() {
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public static List<Human> getChildren() {
        return children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }


    public Family(Human mother, Human father, List children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;


    }


    public void addChild(Human human) {
        int i;
        int n = children1.size();
        ArrayList<Human> newChild = new ArrayList<>(n + 1);
        for (i = 0; i < n; i++)
            newChild.add(children1.get(i));
        newChild.add(newChild.size(), human);
        this.children1 = newChild;
        addChild1(human);

    }

    public void addChild1(Human human) {
        int i;
        int n = children.size();
        ArrayList<Human> newChild = new ArrayList<>(n + 1);
        for (i = 0; i < n; i++)
            newChild.add(children.get(i));
        newChild.add(newChild.size(), human);
        this.children = newChild;

    }


    public boolean deleteChild(int index) {
        boolean deleted = false;
        ArrayList<Human> child = new ArrayList<>(children.size() - 1);

        int i = 0;
        try {
            for (int j = 0; j < children.size(); j++) {
                if (j != index) {
                    child.add(i++, children.get(j));
                }
            }
            if (index < children.size()) {
                deleted = true;
            }
            this.children = child;
            children1 = child;

            return deleted;
        } catch (
                Exception exception) {

            System.out.println("error");
            return false;
        }

    }

    public int sizeAfter;

    public int countFamily() {
        return 2 + children1.size();
    }


    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + getChildren1() +
                '}';
    }
}


package com;

import java.util.HashMap;
import java.util.Map;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    Family family;
    Map<String, Integer> schedule = new HashMap<>();

    public void welcome() {
        System.out.println("Hello," + family.getPet().nickname);
    }

    public void favouritePet() {
        System.out.print("I have a " + family.getPet().getSpecies() + "," + "he is " + family.getPet().age +
                " years old,he is ");
        if (iq < 50) {
            System.out.println("almost not sly");
        } else if (iq > 50) {
            System.out.println("very sly");
        }
    }

    public void feed() {
    }


    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                '}';
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;

    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Human() {
    }

    public Human(String name, String surname, int year, int iq, Map schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        if (iq <= 100 || iq >= 1) {
            this.iq = iq;
        } else {
            System.out.println("Your number is higher or smaller than 100 and 1,respectively.So,iq level is set 99 by system");
            this.iq = 99;
        }

        this.schedule = schedule;
    }
}


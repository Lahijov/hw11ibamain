package com.Service;

import com.DAO.CollectionFamilyDao;
import com.DAO.FamilyDao;
import com.Family;
import com.Human;
import com.Pet;

import java.util.ArrayList;
import java.util.List;

public class FamilyService implements FamilyDao {

    private static FamilyDao collectionFamilyDao = new CollectionFamilyDao();


    @Override
    public void displayAllFamilies() {
        collectionFamilyDao.displayAllFamilies();
    }

    @Override
    public void countFamiliesWithMemberNumber(int number) {
        collectionFamilyDao.countFamiliesWithMemberNumber(number);

    }


    @Override
    public List<Family> getFamiliesBiggerThan(int number) {
        collectionFamilyDao.getFamiliesBiggerThan(number);
        return null;
    }

    @Override
    public List<Family> getFamiliesLessThan(int number) {
        collectionFamilyDao.getFamiliesLessThan(number);
        return null;
    }

    @Override
    public void save(ArrayList<Family> families) {
        collectionFamilyDao.save(families);

    }


    @Override
    public boolean deleteFamily(Family family) {
        return collectionFamilyDao.deleteFamily(family);
    }

    @Override
    public boolean deleteAllChildrenOlderThan(int number) {
        return collectionFamilyDao.deleteAllChildrenOlderThan(number);
    }


    @Override
    public Family getFamilyById(int number) {
        return collectionFamilyDao.getFamilyById(number);
    }

}

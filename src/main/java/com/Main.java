package com;

import com.Controller.FamilyController;
import com.DAO.CollectionFamilyDao;
import com.Service.FamilyService;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Family> families = new ArrayList<>();
        Human mother = new Human("Sevda", "Lahijov", 2002);
        Human father = new Human("Natig", "Lahijov", 1891);
        Family family = new Family(mother, father);
        family.addChild(new Human("Rafq", "Lahijov", 3));
        family.addChild(new Human("Samir", "Lahijov", 1));
        family.addChild(new Human("ss", "Lahijov", 53));
        family.addChild(new Human("ssascs", "Lahijov", 21));


        Human mother2 = new Human("elnure", "Seymurov", 11);
        Human father2 = new Human("Cavid", "Seymurov", 1891);
        Family family2 = new Family(mother2, father2);
        family2.addChild(new Human("cavid", "Seymurov", 5));

        families.add(family);
        families.add(family2);


        FamilyController collectionFamilyDao = new FamilyController();
        collectionFamilyDao.save(families);

        System.out.println("/////////////////////////////////////////////////////////////////////////////////////////////");

        //Printing family that size is bigger than specific number
        collectionFamilyDao.getFamiliesBiggerThan(4);
        System.out.println("/////////////////////////////////////////////////////////////////////////////////////////////");

        //Printing family that size is less than specific number
        collectionFamilyDao.getFamiliesLessThan(4);
        System.out.println("/////////////////////////////////////////////////////////////////////////////////////////////");

        //Removing children
        collectionFamilyDao.deleteAllChildrenOlderThan(10);
        System.out.println("Children that little than 10 years");
        family.children.forEach(System.out::println);
        System.out.println("/////////////////////////////////////////////////////////////////////////////////////////////");

        //Displaying member family
        collectionFamilyDao.displayAllFamilies();
        System.out.println("/////////////////////////////////////////////////////////////////////////////////////////////");


        //Displaying count of family that equal to given number
        collectionFamilyDao.countFamiliesWithMemberNumber(3);
        System.out.println("/////////////////////////////////////////////////////////////////////////////////////////////");


    }


}
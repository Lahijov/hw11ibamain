package com.DAO;

import com.Family;
import com.Human;

import java.util.ArrayList;
import java.util.List;

public interface FamilyDao {

    void displayAllFamilies();


    void save(ArrayList<Family> families);

    List<Family> getFamiliesBiggerThan(int number);

    List<Family> getFamiliesLessThan(int number);

    void countFamiliesWithMemberNumber(int number);

    boolean deleteFamily(Family family);

    boolean deleteAllChildrenOlderThan(int number);

    Family getFamilyById(int number);


}

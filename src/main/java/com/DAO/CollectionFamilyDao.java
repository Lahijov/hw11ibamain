package com.DAO;

import com.Family;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CollectionFamilyDao implements FamilyDao {


    private List<Family> databaseFamily = new ArrayList<>();


    @Override
    public boolean deleteFamily(Family family) {
        try {
            databaseFamily.remove(family);
            return true;
        } catch (Exception e) {
            System.out.println("There is not any family");
            return false;
        }


    }


    @Override
    public Family getFamilyById(int number) {
        return databaseFamily.get(number);

    }


    @Override
    public void displayAllFamilies() {
        databaseFamily.forEach(p -> System.out.println(p));


    }

    @Override
    public List<Family> getFamiliesBiggerThan(int number) {
        System.out.println(databaseFamily.stream().filter(item -> item.countFamily() > number).collect(Collectors.toList()));

        return null;

    }

    public void save(ArrayList<Family> families) {
        databaseFamily = families;

    }

    @Override
    public List<Family> getFamiliesLessThan(int number) {

        System.out.println(databaseFamily.stream().filter(item -> item.getChildren1().size() + 2 < number).collect(Collectors.toList()));

        return null;
    }

    @Override
    public void countFamiliesWithMemberNumber(int number) {
        System.out.println(databaseFamily.stream().filter(item -> item.getChildren1().size() + 2 == number).count());


    }


    public boolean deleteAllChildrenOlderThan(int number) {


        for (int i = 0; i < Family.getChildren().size(); i++) {
            int finalI = i;
            Family.getChildren().removeIf(x -> x.getYear() > number);
        }

        return false;
    }


}







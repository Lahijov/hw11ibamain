package com.Controller;

import com.DAO.FamilyDao;
import com.Family;
import com.Human;
import com.Pet;
import com.Service.FamilyService;

import java.util.ArrayList;
import java.util.List;

public class FamilyController implements FamilyDao {
    FamilyService familyService = new FamilyService();


    @Override
    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    @Override
    public void save(ArrayList<Family> families) {
        familyService.save(families);

    }


    @Override
    public List<Family> getFamiliesBiggerThan(int number) {
        familyService.getFamiliesBiggerThan(number);
        return null;
    }

    @Override
    public List<Family> getFamiliesLessThan(int number) {
        familyService.getFamiliesLessThan(number);
        return null;
    }

    @Override
    public void countFamiliesWithMemberNumber(int number) {
        familyService.countFamiliesWithMemberNumber(number);


    }

    @Override
    public boolean deleteFamily(Family family) {
        return false;
    }

    @Override
    public boolean deleteAllChildrenOlderThan(int number) {
        return familyService.deleteAllChildrenOlderThan(number);
    }


    @Override
    public Family getFamilyById(int number) {
        return familyService.getFamilyById(number);
    }


}

